#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test code for exploring data
Note: used in Sypder3 for preprocessing section of code.
Will fail if ran at once

"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import random



class machiene_learning_analysis:
    def __init__(self,path_to_data):
        self.path_to_data = path_to_data
        self.load()
        self.y_col = []
        self.X_cols = []
        self.X_train,self.X_test, self.y_train, self.y_test = 0,0,0,0


    def load(self):
        ##############################################################
        #                       Importing data                                                                                                    #
        ##############################################################
        self.all_data = pd.read_csv(self.path_to_data)
        self.working_data = self.all_data.copy()        ### Data sets are small so memory is not an issue

    def plot(self,xcols=[],ycols=[]):
        if not xcols:
            xcols = list(self.working_data.columns)
        if not ycols:
            ycols = list(self.working_data.columns)


        for x_col_nm in xcols:
            for y_col_nm in ycols:
                if x_col_nm != y_col_nm:
                    x = self.working_data[x_col_nm]
                    y = self.working_data[y_col_nm]
                    plt.scatter(x, y)
                    plt.xlabel(x_col_nm)
                    plt.ylabel(y_col_nm)
                    plt.show()


    def set_X(self,columns=[]):
        if not columns:
            columns = list(self.working_data.columns)
        self.X_cols = columns
        cols=  self.X_cols + self.y_col +['Test']
        #print(cols)
        self.working_data[cols]

    def set_y(self,column):
        self.y_col = [column]
        cols=  self.X_cols + self.y_col + ['Test']
        #print(cols)
        self.working_data[cols]

    def gen_pca(self,new_col_name ,columns = [ ],num_pcas=1):
        from sklearn.decomposition import PCA
        if not columns:
            columns = list(self.working_data.columns)
        pca = PCA(n_components=num_pcas)
        calculated_pca = pca.fit_transform(self.working_data[columns].values)
        for p in range(num_pcas):
            print(len(calculated_pca[:0]))
            self.working_data["%s_PCA%s"%(new_col_name,p)] = calculated_pca[:0]

    def clean_data(self,rm_na = True):
        ##############################################################
        #      Clean Data, split and normalise                       #
        ##############################################################
        cols = self.X_cols + self.y_col
        if cols:
            self.working_data = self.working_data[cols+["Test"]]
        if rm_na:
            self.working_data = self.working_data.dropna(axis=0)
        for entery in cols:
            self.working_data = self.working_data[self.working_data[entery] != 0]
            if  entery in ["RGB_green","RGB_red","RGB_blue"]:
                self.working_data = self.working_data[self.working_data[entery] < 250]
            elif entery in ["multi_green","multi_red","multi_red_edge","multi_nir"]:
                self.working_data = self.working_data[self.working_data[entery] < 65470]



    def param_range(self,mn,mx,nm):
        sample = []
        for r in range(nm):
            sample.append(random.uniform(mn, mx))
        sample.sort()
        return(sample)


    def k_fold(self,ml_test):
        from sklearn.model_selection import cross_val_score
        if ml_test == "Random Forrest":
            from sklearn.ensemble import RandomForestRegressor
            forest = RandomForestRegressor(n_estimators=1000, criterion='mse', random_state=1, n_jobs=-1)
            scores = cross_val_score(estimator=forest, X=self.X_train, y=self.y_train, cv=10, n_jobs=1)
            print('CV accuracy scores: %s' % scores)


    def test_model(self,model_type,test,param={},outfolder=""):

        #######################################################################
        #                           Regression using SVM                      #
        #######################################################################
        from sklearn.svm import SVR
        from sklearn.preprocessing import StandardScaler
        from sklearn.ensemble import RandomForestRegressor
        from sklearn.metrics import mean_squared_error,r2_score
        from sklearn.neural_network import MLPRegressor
        from sklearn.model_selection import train_test_split
        del self.working_data['Test']

        self.X_train,self.X_test, self.y_train, self.y_test = train_test_split(self.working_data[self.X_cols].values,
                                                                            self.working_data[self.y_col].values.ravel(),
                                                                            test_size=0.3,)


        stdsc = StandardScaler()
        X_train_std = stdsc.fit_transform(self.X_train)   #first fit then transforms
        X_test_std = stdsc.transform(self.X_test)
        #y_train_std = stdsc.transform(self.y_train)
        #y_test_std = stdsc.transform(self.y_test)

        if model_type =="SVR":
            model = SVR()
        elif model_type =="RF":
            model = RandomForestRegressor(criterion='mse')
        elif model_type == "NN":
            print("test1")
            model = MLPRegressor()
        else:
            print("Enter 'NN', 'SVR' or 'RF'")
            return()
        print(test)
        if test == "GS":
            print("test2")
            from sklearn.model_selection import GridSearchCV
            self.gs = GridSearchCV(estimator=model, param_grid=param,
                                    scoring='neg_mean_squared_error',
                                     cv=10, n_jobs=-1,return_train_score=True,
                                     verbose=1)

            self.gs.fit(X_train_std,self.y_train)

        elif test == "VC": #"Valadation Curve"
            param_nm = list(param)[0] ### Will only check the first paramater in dictionary
            from sklearn.model_selection import validation_curve
            #param_range = [0.001, 0.01, 0.1, 1.0, 10.0, 100.0]
            train_scores, test_scores = validation_curve(estimator=model,
                                        X=X_train_std,y=self.y_train,
                                        param_name=param_nm,#####TODO
                                        param_range=param[param_nm],
                                        cv=10)
            train_mean = np.mean(train_scores, axis=1)
            train_std = np.std(train_scores, axis=1)
            test_mean = np.mean(test_scores, axis=1)
            test_std = np.std(test_scores, axis=1)
            plt.plot(param, train_mean,
            color='blue', marker='o',
            markersize=5, label='training accuracy')
            plt.fill_between(param, train_mean + self.train_std,
            train_mean - self.train_std, alpha=0.15,
            color='blue')
            plt.plot(param, test_mean,color='green', linestyle='--',
            marker='s', markersize=5,label='validation accuracy')
            plt.fill_between(param, test_mean + test_std,
            test_mean - test_std, alpha=0.15, color='green')
            plt.grid()
            plt.xscale('log')
            plt.legend(loc='lower right')
            plt.xlabel('Parameter %s'%param_nm)
            plt.ylabel('Accuracy')
            #plt.ylim([0.8, 1.03])
            plt.show()
        elif test == "LC":  ### Learning Curve
            from sklearn.model_selection import learning_curve
            learning_curve(estimator=model,X=self.X_train_std,y=self.y_train,
                           train_sizes=param,cv=10)
            train_mean = np.mean(train_scores, axis=1)
            self.train_std = np.std(train_scores, axis=1)
            test_mean = np.mean(test_scores, axis=1)
            test_std = np.std(test_scores, axis=1)
            plt.plot(param, train_mean,
            color='blue', marker='o',markersize=5,
            label='training accuracy')
            plt.fill_between(param,train_mean + self.X_train_std,
            train_mean - self.train_std,alpha=0.15, color='blue')
            plt.plot(param, test_mean,color='green', linestyle='--',
            marker='s', markersize=5,label='validation accuracy')
            plt.fill_between(param,test_mean + test_std,test_mean - test_std,
            alpha=0.15, color='green')
            plt.grid()
            plt.xlabel('Number of training samples')
            plt.ylabel('Accuracy')
            plt.legend(loc='lower right')
            #plt.ylim([0.8, 1.0])
            plt.show()
        else:
            print("Enter 'GS', 'VC' or 'LC'")


    def run_Model(self,model_type,outfolder="",test_size=0.3,out_of_order_test=0):
        #######################################################################
        #                   Regression using Random Forrests                  #
        #######################################################################
        from sklearn.ensemble import RandomForestRegressor
        from sklearn.metrics import mean_squared_error,r2_score
        from math import sqrt
        from sklearn.preprocessing import StandardScaler
        from sklearn.model_selection import train_test_split

        if out_of_order_test:
            self.out_of_date_data = self.working_data[self.working_data["Test"] == out_of_order_test]
            self.working_data = self.working_data[self.working_data["Test"] != out_of_order_test]
            print()
            del self.working_data['Test']
            del self.out_of_date_data['Test']
            self.X_train, self.y_train = self.working_data[self.X_cols].values, self.working_data[self.y_col].values.ravel()
            self.X_test, self.y_test = self.out_of_date_data[self.X_cols].values, self.out_of_date_data[self.y_col].values.ravel()
        else:
            del self.working_data['Test']
            self.X_train,self.X_test, self.y_train, self.y_test = train_test_split(self.working_data[self.X_cols].values,
                                                                            self.working_data[self.y_col].values.ravel(),
                                                                            test_size=0.3,)



        if model_type =="SVR":
            model = SVR()
        elif model_type =="RF":
            model = RandomForestRegressor(n_estimators=400, criterion='mse', n_jobs=-1)
        elif model_type == "NN":
            model = MLPRegressor()
        else:
            print("Enter 'NN', 'SVR' or 'RF'")



        stdsc = StandardScaler()
        X_train_std = stdsc.fit_transform(self.X_train)   #first fit then transforms
        X_test_std = stdsc.transform(self.X_test)
        stdsc = StandardScaler()


        model.fit(X_train_std,self.y_train)

        y_train_pred = model.predict(X_train_std)
        y_test_pred = model.predict(X_test_std)
        title = "%s Results"%self.y_col[0]
        print(title)
        rmse = 'RMSE train: %.3f, test: %.3f' % (sqrt(mean_squared_error(self.y_train, y_train_pred)),
        sqrt(mean_squared_error(self.y_test, y_test_pred)))
        print(rmse)
        r2 = 'R^2 train: %.3f, test: %.3f' % (r2_score(self.y_train, y_train_pred), r2_score(self.y_test, y_test_pred))
        print(r2)

        plt.figure()
        plt.scatter(y_train_pred, y_train_pred - ml.y_train.ravel(), c='steelblue',
        edgecolor='white', marker='o', s=35, alpha=0.9, label='Training data')
        plt.scatter(y_test_pred, y_test_pred - ml.y_test.ravel(), c='limegreen', edgecolor='white',
        marker='s', s=35, alpha=0.9, label='Test data')
        plt.xlabel('Predicted values')
        plt.ylabel('Residuals')
        plt.legend(loc='upper left')
        #plt.hlines(y=0, xmin=-10, xmax=50, lw=2, color='black')
        #plt.xlim([-10, 50])
        #plt.show()

        if outfolder:
            x = "-".join(self.X_cols)
            y = "-".join(self.y_col)
            out_path = "%s/Random Forrests_y-%s_X-%s.jpg"%(outfolder,x,y)
            plt.savefig(out_path)
        else:
            plt.show()
        plt.clf()
        importance_str = ""
        if model_type =="RF":
            importances = model.feature_importances_
            indices = np.argsort(importances)[::-1]

            feat_labels = self.X_cols
            len(feat_labels)
            order_lable = []
            importance_str = "%s Ordered feature importance"%self.y_col[0]
            print(importance_str)
            importance_str2 = ""
            for f in range(self.X_train.shape[1]):
                importance_str2 += "%2d) %-*s %f\n" % (f + 1, 30,
                     feat_labels[indices[f]],
                     importances[indices[f]])
                order_lable.append(feat_labels[indices[f]])
            print(importance_str2)
            importance_str += "\n"
            importance_str += importance_str2

            plt.figure()
            plt.title('Feature Importance to %s'%self.y_col[0])
            plt.bar(range(self.X_train.shape[1]), importances[indices], align='center')
            plt.xticks(range(self.X_train.shape[1]), order_lable, rotation=90)
            plt.xlim([-1, self.X_train.shape[1]])
            plt.tight_layout()
            if outfolder:
                x = "-".join(self.X_cols)
                y = "-".join(self.y_col)
                out_path = "%s/Random Forrests_Importance_y-%s_X-%s.jpg"%(outfolder,x,y)
                plt.savefig(out_path)
            else:
                plt.show()
            plt.clf()

        if outfolder:
            out_path = "%s/Random Forrests_Stats_y-%s_X-%s.txt"%(outfolder,x,y)
            out_string =  title+ "\n\n" +  r2 + "\n" + rmse + "\n" + importance_str
            with open(out_path,"w") as output_writer:
                output_writer.write(out_string)



################################################################################
#                          Set Veriables                                       #
################################################################################
"""
'RGB_EXP', 'RGB_iso'
'RGB_blue', 'RGB_green', 'RGB_red',
 'multi_green', 'multi_green_EXP','multi_green_iso',
 'multi_nir', 'multi_nir_EXP', 'multi_nir_iso',
 'multi_red', 'multi_red_EXP', 'multi_red_iso'
 'multi_red_edge', 'multi_red_edge_EXP', 'multi_red_edge_iso',
"""

home = os.path.expanduser("~")

path_to_data ="%s/Storage/Phd_DATA/CS401_project/small_cal_images/output2/merged.csv"%home
outfolder = "%s/Storage/Phd_DATA/CS401_project/small_cal_images/output2/ML"%home

#ml = machiene_learning_analysis(path_to_data)
#ml.working_data["Test"]
#ml.filter_by_test(1)
y_col_nm = "multi_green"


test_list = [#["RGB_blue",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             #["RGB_green",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             #["RGB_red",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             #["RGB_blue","RGB_green",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             #["RGB_green","RGB_red",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             #["RGB_blue","RGB_red",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             ["RGB_green","RGB_red","RGB_blue",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm]]



for tl in test_list:
    ml = machiene_learning_analysis(path_to_data)
    #ml.set_X(tl)
    #ml.clean_data()


    #ml.working_data = ml.working_data[ml.working_data["Test"]==1]
    #ml.gen_pca("RGB" ,columns = ["RGB_green","RGB_red","RGB_blue"],num_pcas=2)
    #tl = ["RGB__PCA1","RGB__PCA2",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm]
    ml.set_X(tl)
    ml.set_y(y_col_nm)
    ml.clean_data()


    #ml.working_data

    ###############################Random Forrests################################
    model_type = "RF"
    test = "GS"


    param = {'n_estimators':[1,5,10,25,50,100,200,400,600,1000]}
    #ml.test_model(model_type,test,param)
    #ml.gs.best_estimator_
    #ml.gs.cv_results_


    ml.run_Model(model_type,outfolder,out_of_order_test=2)


    #ml.working_data
    #ml.out_of_date_data
    #del self.working_data['Test']

    ############################### Support Verctor Regression#####################
    """
    model_type = "SVR"
    test = "GS"

    c = ml.param_range(1,100,3)
    g = ml.param_range(0.1,1,3)
    e = ml.param_range(0.11,1,3)
    g = ["auto"]


    #938768.633**0.5

    #2**16





    param = {'C':  c,
                'kernel': ['poly','rbf'],
                'gamma' :g,
                'epsilon' :e,
                "degree":[2,3,4]}


    ml.test_model(model_type,test,param)

    ml.gs.best_estimator_
    ml.gs.cv_results_

    """
    ###############################Neural Network################################
    """
    model_type = "NN"
    test = "LC"


    param = {#'hidden_layer_sizes':[(14,)*3,(20,)*3,(100,)*3],
                  "activation":['identity', 'logistic', 'tanh', 'relu'],
                  #,
                  'learning_rate' : ['constant', 'invscaling', 'adaptive'],
                  'learning_rate_init':[0.001,0.01,0.1,1]}
    param = [0.001,0.01,0.1,1]

    ml.test_model(model_type,test,param)

    ml.gs.best_estimator_
    ml.gs.cv_results_

    ml.gs.best_score_
    """



    """
    y= ml.working_data["multi_red"]/(ml.working_data['multi_red_EXP']*ml.working_data['multi_red_iso'])
    x = (ml.working_data["RGB_green"]*ml.working_data["RGB_red"])/(ml.working_data['RGB_EXP']*ml.working_data['RGB_iso'])
    #y= ml.y_train/
    y2 = y[y<1.4]
    x2 = x[y<1.4]

    len(x2)
    #y = x[x<.5]
    #x = x[y<1.5]
    plt.plot(x2,y2,"ro")
    """
