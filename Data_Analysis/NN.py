#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Note: used in Sypder3 for preprocessing section of code.
Will fail if ran at once
"""




from math import sqrt
from sklearn.metrics import mean_squared_error,r2_score
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import random
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
##############################################################################
def clean_data(cols,data,rm_na = True):
##############################################################
#      Clean Data, split and normalise                       #
##############################################################
    if cols:
        data = data[cols]
    if rm_na:
        data = data.dropna(axis=0)
    for entery in cols:
        data = data[data[entery] != 0]
        if  entery in ["RGB_green","RGB_red","RGB_blue"]:
            data = data[data[entery] < 2**8]
        elif entery in ["multi_green","multi_red","multi_red_edge","multi_nir"]:
            data = data[data[entery] < 2**16]
    return(data)



def param_range(mn,mx,nm):
    sample = []
    for r in range(nm):
        sample.append(random.uniform(mn, mx))
    sample.sort()
    return(sample)
##############################################################################


home = os.path.expanduser("~")
path_to_data ="%s/Storage/Phd_DATA/CS401_project/small_cal_images/output2/merged.csv"%home

all_data = pd.read_csv(path_to_data)


y_col = "multi_green"
test_list = ["RGB_blue",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             "RGB_green",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             "RGB_red",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             "RGB_blue","RGB_green",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             "RGB_green","RGB_red",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             "RGB_blue","RGB_red",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm],
             ["RGB_green","RGB_red","RGB_blue",'RGB_EXP','RGB_iso','%s_EXP'%y_col_nm,'%s_iso'%y_col_nm]]
for X_cols in test_list:
    print("###################################################################")
    print(X_cols)




    model = MLPRegressor()




    #############################################################################
    #############################################################################
    #############################################################################
    #############################################################################
    #                    One Day test

    working_data = clean_data(X_cols+[y_col],all_data[all_data["Test"]==1])

    X_train,X_test,y_train,y_test = train_test_split(working_data[X_cols].values,
                                                     working_data[y_col].values.ravel(),
                                                                         test_size=0.3,)

    stdsc = StandardScaler()
    X_train_std = stdsc.fit_transform(X_train)   #first fit then transforms
    X_test_std = stdsc.transform(X_test)


    param = {"activation":['identity', 'logistic', 'tanh', 'relu'],
             'learning_rate' : ['constant', 'invscaling', 'adaptive'],
             'learning_rate_init':[0.001,0.01,0.1,1]}
    gs = GridSearchCV(estimator=model, param_grid=param,
                      scoring='neg_mean_squared_error',
                    cv=10, n_jobs=-1,return_train_score=True,
                                        verbose=1)

    gs.fit(X_train_std,y_train)


    gs.best_estimator_


#############################################################################
#############################################################################
#############################################################################
#############################################################################
#                    Out of Day Testing Day test
    Test Date

    working_data = clean_data(X_cols+[y_col],all_data[all_data["Test"]!=1])
    out_of_date_data = clean_data(X_cols+[y_col],all_data[all_data["Test"]==1])


    X_train, y_train = working_data[X_cols].values, working_data[y_col].values.ravel()
    X_test, y_test = out_of_date_data[X_cols].values, out_of_date_data[y_col].values.ravel()

    stdsc = StandardScaler()
    X_train_std = stdsc.fit_transform(X_train)   #first fit then transforms
    X_test_std = stdsc.transform(X_test)


    model = MLPRegressor(activation='relu', alpha=0.0001, batch_size='auto', beta_1=0.9,
       beta_2=0.999, early_stopping=False, epsilon=1e-08,
       hidden_layer_sizes=(100,), learning_rate='constant',
       learning_rate_init=0.1, max_iter=200, momentum=0.9,
       nesterovs_momentum=True, power_t=0.5, random_state=None,
       shuffle=True, solver='adam', tol=0.0001, validation_fraction=0.1,
       verbose=False, warm_start=False)



    model.fit(X_train_std,y_train)

    y_train_pred = model.predict(X_train_std)
    y_test_pred = model.predict(X_test_std)
    title = "%s Results"%y_col
    print(title)
    rmse = 'RMSE train: %.3f, test: %.3f' % (sqrt(mean_squared_error(y_train, y_train_pred)),
    sqrt(mean_squared_error(y_test, y_test_pred)))
    print(rmse)
    r2 = 'R^2 train: %.3f, test: %.3f' % (r2_score(y_train, y_train_pred), r2_score(y_test, y_test_pred))
    print(r2)
