# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 15:42:15 2016

@author: Administrator
"""

import numpy as np
import cv2
import os
import fnmatch

"""
Code is based on https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_calib3d/py_calibration/py_calibration.html
"""

class generate_lense_correction_matrix:
    def __init__(self,project_folder,output_folder,grid_size):
        self.project_folder = project_folder
        self.output_folder = output_folder
        self.grid_size = grid_size
        self.matrix_data = {"multi_green":{"objpoints":[],"imgpoints":[],"cnt":0},
                            "multi_nir":{"objpoints":[],"imgpoints":[],"cnt":0},
                            "multi_red":{"objpoints":[],"imgpoints":[],"cnt":0},
                            "multi_red_edge":{"objpoints":[],"imgpoints":[],"cnt":0},
                            "RGB":{"objpoints":[],"imgpoints":[],"cnt":0}}

    def run(self):
        self.search_for_project_images()
        for self.image_group  in self.matches:
            for self.image_type in self.matches[self.image_group]:
                self.image_location = self.matches[self.image_group][self.image_type]["path"]
                self.load_image()
                self.search_for_chessboard_corners()
        self.gen_matrix_and_wight_data()

    def search_for_project_images(self):
        filter_strings = {'GRE':"multi_green",
        'NIR':"multi_nir",
        'RED':"multi_red",
        'REG':"multi_red_edge",
        'RGB':"RGB"}
        self.matches = {}
        for root, dirnames, filenames in os.walk(self.project_folder):
            for filter_string in filter_strings:
                for filename in fnmatch.filter(filenames, "*%s*"%filter_string) :
                    if filename[:-8] not in self.matches.keys():
                        self.matches[filename[:-8]] = {}
                    self.matches[filename[:-8]][filter_strings[filter_string]]= {"path":  (os.path.join(root, filename))}

    def load_image(self):
        print("reading image")
        print(os.path.basename(self.image_location))
        self.img_origional = cv2.imread(self.image_location,-1)
        print("image Loaded")
        if self.image_type == "RGB":
            #self.image_scaled = 0.25
            self.img_gray = cv2.cvtColor(self.img_origional,cv2.COLOR_BGR2GRAY)
            #self.img_gray = cv2.resize(self.img_gray, (0,0), fx=self.image_scaled, fy=self.image_scaled)
            self.matches[self.image_group][self.image_type]["shape"] = self.img_gray.shape[::-1]
        else:
            self.image_location
            self.img_gray = (self.img_origional/256).astype('uint8')
            self.matches[self.image_group][self.image_type]["shape"] = self.img_gray.shape[::-1] ## TEMP map cause errors

    def search_for_chessboard_corners(self):
        print("decting cornors")
        ret, corners = cv2.findChessboardCorners(self.img_gray, (self.grid_size[0],self.grid_size[1]),None)
        mean_value = self.img_gray.mean()
        steps = (255 - mean_value)/4
        tresholds = [mean_value,mean_value+steps,mean_value+steps*2,mean_value+steps*3,mean_value+steps*4]
        cnt= 0
        objp = np.zeros((self.grid_size[0]*self.grid_size[1],3), np.float32)
        objp[:,:2] = np.mgrid[0:self.grid_size[0],0:self.grid_size[1]].T.reshape(-1,2)
        while not ret and cnt < 5:
            _,self.img_gray = cv2.threshold(self.img_gray,tresholds[cnt],255,cv2.THRESH_TOZERO)
            ret, corners = cv2.findChessboardCorners(self.img_gray, self.grid_size,None)
            cnt+=1
        # Arrays to store object points and image points from all the images.
        if ret == True:
            self.matrix_data[self.image_type]["objpoints"].append(objp)
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
            corners2 = cv2.cornerSubPix(self.img_gray,corners,self.grid_size,(-1,-1),criteria)
            self.matrix_data[self.image_type]["imgpoints"].append(corners2)
            self.matrix_data[self.image_type]["cnt"] +=1

    def gen_matrix_and_wight_data(self):
        for image_type in self.matrix_data:
            cnt = self.matrix_data[image_type]["cnt"]
            print("generating martix for %s from %s images where chessboard was found."%(image_type,cnt))
            shape = self.matches[self.image_group][image_type]["shape"] #Relies on the last group
            imgpoints = self.matrix_data[image_type]["imgpoints"]
            objpoints = self.matrix_data[image_type]["objpoints"]
            ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, shape,None,None)
            print("Calcaulating Error")
            total_error = 0
            for i in range(len(objpoints)):
                imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
                error = cv2.norm(imgpoints[i],imgpoints2, cv2.NORM_L2)/len(imgpoints2)
                total_error += error
            mean_error = total_error/len(objpoints)
            print("Wrighting Data")
            out_path = "%s/%s_correction_error.csv"%(self.output_folder,image_type)
            with open(out_path, "w") as text_file:
                text_file.write("Error: %s" % mean_error)
            out_path = "%s/%s_correction_cameraMatrix.csv"%(self.output_folder,image_type)
            np.savetxt(out_path, mtx ,delimiter=",")
            out_path = "%s/%s_correction_distCoeffs.csv"%(self.output_folder,image_type)
            np.savetxt(out_path, dist ,delimiter=",")


home = os.path.expanduser("~")
project_folder = ""%home
output_folder =  ""%home
grid_size = (11,16) ### Always one less than created grid

gl = generate_lense_correction_matrix(project_folder,output_folder,grid_size)
gl.run()
