import os
import subprocess

class extract_exif():
    def __init__(self,folder_path,capture=False,images_for_data_exteact=[]):
        self.folder_path = folder_path
        self.EXIF_data = {}
        self.capture = capture
        self.images_for_data_exteact = images_for_data_exteact

    def run(self):
        if self.capture:
            self.unzip_folder()
        self.read_exif_data()

    def read_exif_data(self):
        image = ""
        if self.capture:
            image_folder_path = "%s/extracted"%(self.folder_path)
            exif_images = {image_folder_path:os.listdir("%s/extracted"%self.folder_path)}
        else:
            image_folder_path = self.folder_path
            exif_images = {image_folder_path:self.images_for_data_exteact}
        for f in exif_images[image_folder_path]:
            if "GRE" in f:
                image= "green"
                self.EXIF_data[image] = {}
            elif "RED" in f:
                image="red"
                self.EXIF_data[image] = {}
            elif "REG" in f:
                image="red_edge"
                self.EXIF_data[image] = {}
            elif "NIR" in f:
                image="near_infrared"
                self.EXIF_data[image] = {}
            elif "RGB" in f:
                image="rgb"
                self.EXIF_data[image] = {}
            else:
                continue
            print(image)
            cmd = ["exiv2","-p","a", "%s/%s"%(image_folder_path,f)]
            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
            temp_exif_data = proc.stdout.read()
            split_exif_lines = str(temp_exif_data).split("\\n")
            if len(split_exif_lines) < 2:
                split_exif_lines = str(temp_exif_data).split("\n")
            for exif_line in split_exif_lines:
                ordered_exif_data = exif_line.split()
                if ordered_exif_data:
                    if ordered_exif_data[0] == "Exif.Photo.ISOSpeedRatings":
                        ev_cleaned = ordered_exif_data[3].replace("\\","").replace("'","").replace('"','')
                        self.EXIF_data[image]["iso"] = int(ev_cleaned)
                    elif ordered_exif_data[0] == "Exif.Photo.ExposureTime":
                        ev_cleaned = ordered_exif_data[3].replace("\\","").replace("'","").replace('"','')
                        if "/" in ev_cleaned:
                            num,denom = ev_cleaned.split("/")
                            ev_cleaned = float(num)/float(denom)
                        ev_cleaned = int(float(ev_cleaned)*1000000) # convert to micro seconds
                        self.EXIF_data[image]["exp_us"] = ev_cleaned

    def unzip_folder(self):
        zip = "calibration_images.zip"
        cmd = ["unzip","-d","%s/extracted"%self.folder_path,"%s/%s"%(self.folder_path,zip)]
        subprocess.call(cmd)
