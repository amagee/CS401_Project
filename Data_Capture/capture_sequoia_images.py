#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  2 13:39:02 2018

@author: administrator
"""

import requests
import json
import sys, shutil,os
import time
from extract_exif_data import extract_exif

class capture_multi_exposure_images:
    def __init__(self):
        self.sequoia_ip = '192.168.47.1'
        self.exp_ranges = [0.5,0.75,1,1.75,2]
        self.iso_ranges = [0.5,0.75,1,1.75,2]
        self.time_delay = 1 ## Time delay between images to enuse camera is ready
        self.temp_folder = "temp"

    def run(self):
        self.prepare_camera("single")
        acceptable_answers = {"n":["no","No","N","n","Quit","Q","q"]}
        message = """Capture Images: default = Yes\nenter values no,No,N,n,Quit,Q or q to Quit"""
        self.get_base_camera_pram()
        while True:
            time.sleep(2)
            self.prepare_camera("timelapse")
            capture = self.ask_question(message,acceptable_answers)
            if capture  == "y":
                self.capture_images_at_differing_exposures()
            else:
                break

    def ask_question(self,question,acceptable_answers,default_answer = "y"):
        #acceptable_answers is a dictionary where  = {<results>:<list of acccetable answers>}
        print(question)
        answer  = input("Answer: ")
        if acceptable_answers == "float":
            try:
                float_answer = float(answer)
                return(float_answer)
            except:
                print("Please enter float")
                return(None)
        else:
            for result in acceptable_answers:
                if answer in acceptable_answers[result]:
                    capture = result
                    break
            else:
                capture = default_answer
            return(capture)


    def send_request_to_camera(self,get_or_post,request_path,request_message="",inital=False):
        try:
            if get_or_post == "get":
                r = requests.get('http://' + self.sequoia_ip + request_path)
            else:
                r = requests.post('http://' + self.sequoia_ip + request_path, json = request_message)
        except requests.exceptions.RequestException:
            print("Could not connect to camera")
            if inital==True:
                print("Camera is no connected")
                print("Connect Camera and press Enter or type Q and enter to Quit: ")
                if input().lower().strip() == "q":
                    sys.exit()
                return("")
            else:
                print("Connection to Camera was lost while processing")
                print("Exiting program")
                sys.exit()
        return(r)

    def prepare_camera(self,capture_mode):
        print("###################################################")
        print(capture_mode)
        print("###################################################")
        r = ""
        while not r:
            r = self.send_request_to_camera("get",'/capture',inital=True)
        capture_settings = { "capture_mode": capture_mode,
                    "timelapse_param": 2.5,
                    "gps_param": 25.0,
                    "overlap_param": 80.0,
                    "resolution_rgb": 16,
                    "resolution_mono": 1.2,
                    "bit_depth": 10,
                    "sensors_mask": 31,
                    "storage_selected": "internal",
                    "auto_select": "off",}

        r = self.send_request_to_camera("put",'/config',capture_settings)

        print(json.dumps(r.json(), indent = 4))

    def captue_calibration_image(self):
        if not os.path.exists(self.temp_folder):
            os.mkdir(self.temp_folder)
        camera_auto_settings  = { "monochrome": {
                        "mode": "auto",
                        "green": {
                            "exp_us": 12000,
                            "iso": 800,
                        },
                        "red": {
                            "exp_us": 13000,
                            "iso": 200,
                        },
                        "red_edmessagege": {
                            "exp_us": 14000,
                            "iso": 400,
                        },
                        "near_infrared": {
                            "exp_us": 15000,
                            "iso": 100,
                        },
                    },
                    "rgb": {
                        "mode": "auto",
                        "exp_us": 2530,
                        "iso": 200,
                    },
                  }

        self.send_request_to_camera("post",'/manualmode',camera_auto_settings)
        time.sleep(1)
        #self.send_request_to_camera(self,"post",'/manualmode',camera_auto_settings)
        pre_files = self.send_request_to_camera("get", '/file/internal').json()
        self.send_request_to_camera("get", '/capture/start')
        time.sleep(4)
        post_files = self.send_request_to_camera("get", '/file/internal').json()
        for f in post_files:
            if f not in pre_files.keys():
                download_path = "http://" + self.sequoia_ip +"/download/" + f
                r = requests.get(download_path, stream=True)
                if r.status_code == 200:
                    with open('%s/calibration_images.zip'%self.temp_folder, 'wb') as f:
                        r.raw.decode_content = True
                        shutil.copyfileobj(r.raw, f)
                break

    def get_base_camera_pram(self):
        acceptable_answers = {"n":["no","No","N","n","Quit","Q","q"],
                               "s":["Skip","skip","s","S"]}
        message = """Capture Calibration Image: default = Yes\nenter 'Skip','skip','s' or 'S' to use default values\nenter values no,No,N,n,Quit,Q or q to Quit"""
        capture = self.ask_question(message,acceptable_answers)
        if capture == "y":
            self.captue_calibration_image()
            ee = extract_exif(self.temp_folder,capture=True)
            ee.run()
            EXIF_data = ee.EXIF_data
        elif capture == "s":
            EXIF_data  = {'rgb': {'exp_us': 40000, 'iso': 1593},
                        'near_infrared': {'exp_us': 3003, 'iso': 796},
                        'green': {'exp_us': 3003, 'iso': 796},
                        'red': {'exp_us': 3003, 'iso': 796},
                        'red_edge': {'exp_us': 3003, 'iso': 796}}
        elif capture == "n":
            sys.exit()


        self.camera_base_settings = {"monochrome": {
                                    "green": {
                                        "exp_us": EXIF_data["green"]["exp_us"],
                                        "iso": EXIF_data["green"]["iso"],
                                    },
                                    "red": {
                                        "exp_us": EXIF_data["red"]["exp_us"],
                                        "iso": EXIF_data["red"]["iso"],
                                    },
                                    "red_edge": {
                                        "exp_us": EXIF_data["red_edge"]["exp_us"],
                                        "iso": EXIF_data["red_edge"]["iso"],
                                    },
                                    "near_infrared": {
                                        "exp_us": EXIF_data["near_infrared"]["exp_us"],
                                        "iso": EXIF_data["near_infrared"]["iso"],
                                    },
                                        },
                                "rgb": {
                                        "exp_us": EXIF_data["rgb"]["exp_us"],
                                        "iso": EXIF_data["rgb"]["iso"],
                                        }
                                        }


    def update_camera_pram(self,iso_exp_settings):

        self.camera_pram =  {"monochrome":{"mode": "manual"},
                        "rgb":{"mode": "manual"}}
        for sensor_type in self.camera_base_settings:
            if sensor_type == "monochrome":
                exp_us_scalor = iso_exp_settings["Multispec"]["Exposure"]
                iso_scalor = iso_exp_settings["Multispec"]["ISO"]
                print("Setting Multispec camera partamates => exposure : %s, ISO : %s"%(exp_us_scalor,iso_scalor))
                for camera in self.camera_base_settings[sensor_type]:
                    new_exp_us = int(self.camera_base_settings[sensor_type][camera]["exp_us"]*exp_us_scalor)
                    new_iso = int(self.camera_base_settings[sensor_type][camera]["iso"]*iso_scalor)
                    self.camera_pram[sensor_type][camera] = {}
                    self.camera_pram[sensor_type][camera]["exp_us"] = new_exp_us
                    self.camera_pram[sensor_type][camera]["iso"] = new_iso
            else:
                exp_us_scalor = iso_exp_settings["RGB"]["Exposure"]
                iso_scalor = iso_exp_settings["RGB"]["ISO"]
                print("Setting RGB camera partamates => exposure : %s, ISO : %s"%(exp_us_scalor,iso_scalor))
                new_exp_us = int(self.camera_base_settings[sensor_type]["exp_us"]*exp_us_scalor)
                new_iso = int(self.camera_base_settings[sensor_type]["iso"]*iso_scalor)
                self.camera_pram[sensor_type]["exp_us"] = new_exp_us
                self.camera_pram[sensor_type]["iso"] = new_iso

    def capture_images_at_differing_exposures(self):
        print("Select Exposture and iso setting")
        acceptable_answers={}
        answer_filter ={}
        iso_exp_settings ={"RGB":{"Exposure":None, "ISO":None},
                            "Multispec":{"Exposure":None ,"ISO":None}}
        for sen in iso_exp_settings:
            for set in iso_exp_settings[sen]:
                while not iso_exp_settings[sen][set]:
                    message = "Set %s  scalor for %s :"%(sen,set)
                    acceptable_answers = "float"
                    ans = self.ask_question(message,acceptable_answers)
                    iso_exp_settings[sen][set] = ans
                    if ans == 0:
                        break
        self.update_camera_pram(iso_exp_settings)
        self.capture_image()


    def capture_image(self):
        # send camera paramaters to Sequoia
        self.send_request_to_camera("post",'/manualmode',self.camera_pram)
        # start capture
        input("Press  enter to Start")
        print("Capturing Image")
        self.send_request_to_camera("get",'/capture/start')
        capture_status = "Busy"
        input("Press enter to Finish")
        self.send_request_to_camera("get",'/capture/stop')


################################################################################
#                          Capture Data                                        #
################################################################################
cap = capture_multi_exposure_images()
cap.run()
print("Capturing_finished")
#input("Press enter to Exit")
