# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import cv2
import numpy as np
from random import randint
import csv,os

class generate_chess_board:
    def __init__ (self,width,height,grid_size,number_of_grids=1,grid_colour = "gray",min_val=0,max_val = 255,output_folder="."):
        self.output_folder = output_folder
        self.width = width
        self.height = height
        self.grid_size = grid_size
        self.min_val = min_val
        self.max_val = max_val
        self.grid_colour = grid_colour
        self.number_of_grids  = number_of_grids
        self.generated_grids = []

    def run(self):
        for self.grid_number in range(1,self.number_of_grids+1):
            self.gen_grid_colours()
            self.gen_grid()
        self.merge_grids()
        self.wright_out_data()


    def gen_grid_colours(self):
        base_colours={"gray":[1,1,1],
                     "red":[0,0,1],
                     "green":[0,1,0],
                     "blue":[1,0,0]}
        base_colour = base_colours[self.grid_colour]

        gray_scale = (((self.max_val-self.min_val)/self.number_of_grids)*self.grid_number)
        b = int(base_colour[0]*gray_scale+self.min_val)
        g = int(base_colour[1]*gray_scale+self.min_val)
        r = int(base_colour[2]*gray_scale+self.min_val)
        self.created_grid_colour = (b,g,r)


    def gen_grid(self):
        ### Add two for the border
        number_of_rows = self.grid_size[0] +2
        number_of_cols = self.grid_size[1] +2
        grid = np.empty((self.height,self.width,3), np.uint8)
        grid.fill(255)
        x_steps = int(width/(number_of_rows))
        y_steps = int(height/(number_of_cols))

        border_values = {"row":[],"col":[]}
        for b in range(1):
            border_values["col"].append(b)
            border_values["row"].append(b)
            border_values["col"].append(number_of_cols-(b+1))
            border_values["row"].append(number_of_rows-(b+1))
        if (self.grid_number/self.number_of_grids) <= 0.5:
            dark_square = (255,255,255)
            light_square =  self.created_grid_colour
        else:
            dark_square = self.created_grid_colour
            light_square = (0,0,0)

        cnt = 0
        self.used_colours = []
        start_white = False
        for row in range(number_of_rows):
            if start_white == True:
                white = True
                start_white = False
            else:
                white = False
                start_white = True
            used_colours_col = []
            for col in range(number_of_cols):
                check1 = row in border_values["row"] or col in border_values["col"]
                if check1:
                        colour = (255,255,255)
                else:
                    extent= [[x_steps*col,y_steps*row],[x_steps*(col+1),y_steps*(row+1)]] ##ll,tr
                    if white != True:
                        colour = light_square
                        white = True
                    else:
                        colour = dark_square
                        white = False
                    grid[extent[0][0]:extent[1][0],extent[0][1]:extent[1][1]] = colour
                    used_colours_col.append(colour)
                cnt+=1
            self.used_colours.append(used_colours_col)
            self.reorginise_used_colour_values()
        grid = grid[int(y_steps/2):int(-y_steps/2), int(x_steps/2):int(-x_steps/2)]


        self.generated_grids.append(grid)


    def merge_grids(self):
        if len(self.generated_grids)%2:
            x_steps = int(width/(self.grid_size[0]+2))
            y_steps = int(height/(self.grid_size[1]+2))
            empty_grid = np.empty((self.height,self.width,3), np.uint8)
            empty_grid.fill(255)
            empty_grid = empty_grid[int(y_steps/2):int(-y_steps/2), int(x_steps/2):int(-x_steps/2)]
            self.generated_grids.append(empty_grid)
        print("########################")
        print(len(self.generated_grids))
        grid_rows = []
        while self.generated_grids:
            g1 = self.generated_grids.pop(0)
            g2 = self.generated_grids.pop(0)
            merged_row = np.concatenate((g1, g2), axis=0)
            grid_rows.append(merged_row)
        self.chess_image = np.concatenate(grid_rows, axis=1)

    def reorginise_used_colour_values(self):
        #transpose and group data
        self.used_colours_output =  []
        cnt = 0
        for col  in self.used_colours:
            self.used_colours_output.append([])
            for row in col:
                bgr_string = "%s:%s:%s"% (row[0],row[1],row[2])
                self.used_colours_output[cnt].append(bgr_string)
            cnt+=1

    def wright_out_data(self):
        file_name = "chessboard_grid_%s_%s_%s_%s_%s"%(self.grid_colour,self.width,self.height,self.grid_size[0],self.grid_size[1]) #TEMP remove min_val
        image_out_path ="%s/%s.jpg"%(self.output_folder,file_name)
        ###SAVE Grid Iamage
        cv2.imwrite(image_out_path,self.chess_image)
        ###SAVE Colour values
        file_out_path ="%s/%s.csv"%(self.output_folder,file_name)## TEMP REOVE MIV VALUE
        with open(file_out_path, 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for row in self.used_colours_output:
                writer.writerow(row)


################################################################################
#                       Set veriables
################################################################################


grid_size = (4,5) #row,column
number_of_grids=8
height = 600
width = int(height/(grid_size[1])*grid_size[0])

max_val = 255
min_val = 100
home = os.path.expanduser("~")
output_folder =  ""%home

#for grid_colour in ["black","set","random","gray"]:
for grid_colour in ["gray","blue","red","green"]:
    gcb = generate_chess_board(width,height,grid_size,number_of_grids,grid_colour,min_val,max_val,output_folder)
    gcb.run()
