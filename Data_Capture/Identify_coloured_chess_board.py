#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  1 21:17:39 2018

@author: scut
"""
import cv2
import math
import imutils
import fnmatch
import os#,csv
import pandas as pd
import numpy as np
from extract_exif_data import extract_exif

class identify_and_extract_chessboard_colours:
    def __init__(self,grid_size,project_folder,correct_image):
        self.grid_size = grid_size
        self.project_folder = project_folder
        self.found_colours = {}
        self.correct_image = correct_image

    def run(self):
        self.search_for_project_images()
        len_matches = len(self.matches)
        match_cnt =1
        for self.image_group  in self.matches:    #### TODO finish from here as the images
            for self.image_type in self.matches[self.image_group]:
                print("Processing ==> %s:%s"%(match_cnt,len_matches))
                self.image_location = self.matches[self.image_group][self.image_type]
                self.load_image()
                if self.search_for_chessboard():
                    self.rotate_image()
                    del self.corners
                    if self.search_for_chessboard(): #search again after image has been rotated
                        self.extract_chessboard_values()
            match_cnt+=1
            if self.image_group in self.found_colours.keys():
                print("Wrighting OUT DATA")
                self.wright_out_data()


    def search_for_project_images(self):
        filter_strings = {'GRE':"multi_green",
        'NIR':"multi_nir",
        'RED':"multi_red",
        'REG':"multi_red_edge",
        'RGB':"RGB"}
        self.matches = {}
        print(self.project_folder)
        for root, dirnames, filenames in os.walk(self.project_folder):
            print(filenames)
            print("##########")
            for filter_string in filter_strings.keys():
                for filename in fnmatch.filter(filenames, "*%s*"%filter_string) :
                    if filename[:-8] not in self.matches.keys():
                        self.matches[filename[:-8]] = {}
                    self.matches[filename[:-8]][filter_strings[filter_string]] =  (os.path.join(root, filename))

    def load_image(self):
        print("reading image %s"%(os.path.basename(self.image_location)))
        self.img_origional = cv2.imread(self.image_location,-1)
        if self.correct_image == True:
            self.correct_image_lense_distortion()
        print("image Loaded")
        if self.image_type == "RGB":
            self.image_scaled = 0.25
            self.img_gray = cv2.cvtColor(self.img_origional,cv2.COLOR_BGR2GRAY)
            self.img_gray = cv2.resize(self.img_gray, (0,0), fx=self.image_scaled, fy=self.image_scaled)
        else:
            self.image_scaled = 0.75
            self.image_location
            self.img_gray = (self.img_origional/256).astype('uint8')
            self.img_gray = cv2.resize(self.img_gray, (0,0), fx=self.image_scaled, fy=self.image_scaled)

    def correct_image_lense_distortion(self):
        print("correct image")
        h,w = self.img_origional.shape[:2]
        # undistort
        mtx = np.loadtxt("correction_matrix/%s_correction_cameraMatrix.csv"%self.image_type, delimiter=',')
        dist = np.loadtxt("correction_matrix/%s_correction_distCoeffs.csv"%self.image_type, delimiter=',')
        newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))
        self.img_origional = cv2.undistort(self.img_origional, mtx, dist, None, newcameramtx)


    def search_for_chessboard(self):

        clip_out_folder = "%stemp"%(self.project_folder)
        print("decting cornors")
        ret, self.corners = cv2.findChessboardCorners(self.img_gray, (self.grid_size[0],self.grid_size[1]),None)
        #mean_value = self.img_gray.mean()
        mean_value = 255/4
        steps = 15
        steps_up = (255 - mean_value)/4
        steps_down = (255 - mean_value)/4
        tresholds = []
        tresh_cnt = 0
        tresh = 0
        while tresh < 230:
            tresh = mean_value + (steps*tresh_cnt)
            tresholds.append(tresh)
            tresh_cnt+=1
        cnt= 0
        while tresholds and not ret:
            tresh = tresholds.pop(0)
            print("treshold")
            _,self.img_gray_tresh = cv2.threshold(self.img_gray,tresh,255,cv2.THRESH_BINARY)
            ret, self.corners = cv2.findChessboardCorners(self.img_gray_tresh, self.grid_size ,None)
            if ret:
                print("Found using treshold")
                break
            print("treshold Inverted")
            _,self.img_gray_tresh = cv2.threshold(self.img_gray,tresh,255,cv2.THRESH_BINARY_INV)
            ret, self.corners = cv2.findChessboardCorners(self.img_gray_tresh, self.grid_size ,None)

            if ret:
                print("Found using treshold")
                break
            cnt+=1
        if not ret:
            print("Failed to detect chessboard")
        return(ret)

    def rotate_image(self):
            cornor = []
            cornor.append((int(self.corners[0][0][0]),int(self.corners[0][0][1])))
            cornor.append((int(self.corners[-1][0][0]),int(self.corners[-1][0][1])))
            ##
            row_diff = cornor[0][1]-cornor[1][1]
            if row_diff > 0:
                cornor1 = cornor[1]
                cornor2 = cornor[0]
            else:
                cornor1 = cornor[0]
                cornor2 = cornor[1]
            col_diff = cornor1[0]-cornor2[0]

            numa = abs(cornor1[0]-cornor2[0])
            denom = abs(cornor1[1]-cornor2[1])
            if denom == 0:
                angle = 0
            else:
                angle = math.atan(numa/denom)
            angle_of_change = abs(math.atan(self.grid_size[1]/self.grid_size[0])*(180/math.pi)-angle*(180/math.pi))
            if col_diff > 0:
                angle_of_change *= -1
            print("Rotating image by : %s"%(angle_of_change))
            self.img_origional = imutils.rotate_bound(self.img_origional, angle_of_change)
            self.img_gray = imutils.rotate_bound(self.img_gray, angle_of_change)

    def extract_chessboard_values(self):
        if self.image_group not in self.found_colours.keys():
            self.found_colours = {self.image_group:{}}
        cnt=0
        skips = [self.grid_size[0]-1]
        for cornor in self.corners:
            if cnt not in skips:
                cornor1 = (int(self.corners[cnt][0][0]/self.image_scaled),int(self.corners[cnt][0][1]/self.image_scaled))
                cornor2 = (int(self.corners[cnt+1+self.grid_size[0]][0][0]/self.image_scaled),int(self.corners[cnt+1+self.grid_size[0]][0][1]/self.image_scaled))
                col_diff = cornor1[0]-cornor2[0]
                row_diff = cornor1[1]-cornor2[1]
                if col_diff < 0:
                    min_col = cornor1[0]
                else:
                    min_col = cornor2[0]
                if row_diff < 0:
                    min_row = cornor1[1]
                else:
                    min_row = cornor2[1]
                col_buffer = abs(col_diff*20/100)
                row_buffer = abs(row_diff*20/100)
                min_x_crop = int(min_col+col_buffer)
                max_x_crop = int(min_col+abs(col_diff)-col_buffer)
                min_y_crop = int(min_row+row_buffer)
                max_y_crop = int(min_row+abs(row_diff)-row_buffer)
                if "output" not in self.found_colours[self.image_group].keys():
                    self.found_colours[self.image_group]["output"] = {}
                    keys =["RGB_blue","RGB_green","RGB_red","multi_green",
                    "multi_nir","multi_red","multi_red_edge","RGB_EXP","RGB_iso",
                    "multi_green_EXP","multi_nir_EXP","multi_red_EXP",
                    "multi_red_edge_EXP","multi_green_iso","multi_nir_iso",
                    "multi_red_iso","multi_red_edge_iso"]
                    for key in keys:
                        self.found_colours[self.image_group]["output"][key] = []

                "%s_iso"%self.image_type
                font = cv2.FONT_HERSHEY_SIMPLEX
                iso,exp = self.get_exif_data()
                if self.image_type == "RGB":
                    crop_img = self.img_origional[min_y_crop:max_y_crop,min_x_crop:max_x_crop,:]
                    #crop_img = img[200:400, 100:300] # Crop from x, y, w, h -> 100, 200, 300, 400
                    # NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]

                    mean_red = np.mean(crop_img[:,:,2]) # maen for each band
                    mean_green = np.mean(crop_img[:,:,1]) # maen for each band
                    mean_blue = np.mean(crop_img[:,:,0]) # maen for each band
                    self.found_colours[self.image_group]["output"]["RGB_blue"].append(mean_blue)
                    self.found_colours[self.image_group]["output"]["RGB_green"].append(mean_green)
                    self.found_colours[self.image_group]["output"]["RGB_red"].append(mean_red)
                    self.found_colours[self.image_group]["output"]["RGB_iso"].append(iso)
                    self.found_colours[self.image_group]["output"]["RGB_EXP"].append(exp)
                else:
                    crop_img = self.img_origional[min_y_crop:max_y_crop,min_x_crop:max_x_crop]
                    mean_dn = np.mean(crop_img) # maen for each band
                    self.found_colours[self.image_group]["output"][self.image_type].append(mean_dn)
                    self.found_colours[self.image_group]["output"]["%s_iso"%self.image_type].append(iso)
                    self.found_colours[self.image_group]["output"]["%s_EXP"%self.image_type].append(exp)

                if cnt > ((self.grid_size[1])-1)*(self.grid_size[0]-1):#Two because of buffer of one on each way
                    break
            else:asonal flu i
                skips += [skips[-1]+self.grid_size[0]]
            cnt+=1


    def get_exif_data(self):
        images_for_data_exteact=[os.path.basename(self.image_location)]
        folder= os.path.dirname(self.image_location)
        ee = extract_exif(folder,capture=False,images_for_data_exteact=images_for_data_exteact)
        ee.run()
        EXIF_data = ee.EXIF_data

        mapping = { "multi_green":"green",
                    "multi_red":"red",
                    "multi_red_edge":"red_edge",
                    "multi_nir":"near_infrared",
                    "RGB":"rgb"}
        if "iso" in EXIF_data[mapping[self.image_type]].keys():
            iso = EXIF_data[mapping[self.image_type]]["iso"]
        else:
            iso = 0
        if "exp_us" in EXIF_data[mapping[self.image_type]].keys():
            exp = EXIF_data[mapping[self.image_type]]["exp_us"]
        else:
            exp = 0
        return(iso,exp)


    def wright_out_data(self):
        bands = ["RGB_blue","RGB_green","RGB_red","multi_red","multi_green","multi_red_edge","multi_nir"]
        if not os.path.exists("%s/output/"%(self.project_folder)):
            os.mkdir("%s/output/"%(self.project_folder))
        file_out_path ="%s/output/%s.csv"%(self.project_folder,self.image_group)
        out_dict = {}

        for band in self.found_colours[self.image_group]["output"]:
            if self.found_colours[self.image_group]["output"][band]:
                est_dark = [self.found_colours[self.image_group]["output"][band][0],self.found_colours[self.image_group]["output"][band][3],self.found_colours[self.image_group]["output"][band][4]]
                est_light = [self.found_colours[self.image_group]["output"][band][1],self.found_colours[self.image_group]["output"][band][2],self.found_colours[self.image_group]["output"][band][5]]
                mean_est_dark = sum(est_dark) / float(len(est_dark))
                mean_est_light = sum(est_light) / float(len(est_light))
                if mean_est_dark > mean_est_light: ## then estmete was correct
                    out_dict[band] =  self.found_colours[self.image_group]["output"][band]
                else:
                    print("%s was inverted"%band)
                    out_dict[band] =  self.found_colours[self.image_group]["output"][band][::-1]
            print(out_dict)
            df = pd.DataFrame(out_dict)
            df["Date"] = "%s-%s-20%s"%(self.image_group[8:10],self.image_group[6:8],self.image_group[4:6])
            df.to_csv(file_out_path,index=False)

    def data_to_one_file(self):
        output_folder ="%s/output"%(self.project_folder)
        file_out_path ="%s/output/all_data.csv"%(self.project_folder)
        all_csv_data = []
        csv_file_nms = os.listdir(output_folder)
        for csv_file_nm in csv_file_nms:
            if csv_file_nm[-4:] == ".csv":
                path_to_data = os.path.join(project_folder,csv_file_nm)
                csv_data = pd.read_csv(path_to_data)
                all_csv_data.append(csv_data)
                df.to_csv(file_out_path,index=False)



################################################################################
####                                Define veriables                         ###
################################################################################



grid_size = (3,4) ### Always one less than created grid
correct_image = True

home = os.path.expanduser("~")


project_folder =  "%s"%home


id_chess = identify_and_extract_chessboard_colours(grid_size,project_folder,correct_image)
id_chess.run()
